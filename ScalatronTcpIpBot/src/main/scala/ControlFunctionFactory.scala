class ControlFunctionFactory extends main.scala.org.kermitas.scalatron.bot.ControlFunctionFactoryTrait
{
  override def create : String => String =
  {
    try
    {
      main.scala.org.kermitas.scalatron.bot.TcpIpBotManager.createNewControlFunction
    }
    catch
    {
      case t : Throwable =>
      {
        println( getClass.getName + ".create: caught throwable " + t )
        t.printStackTrace
        throw t
      }
    }
  }
}