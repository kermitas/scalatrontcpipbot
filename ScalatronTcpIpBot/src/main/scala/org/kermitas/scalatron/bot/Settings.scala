package main.scala.org.kermitas.scalatron.bot
{
  object Settings
  {
    import java.util.logging._

    // -------------------

    protected val log = Logger.getLogger( getClass.getName )
    protected var properties = loadProperies( "main.scala.org.kermitas.scalatron.bot.tcpip.Settings" , "settings.conf" )
    configureLogging( Level.parse( properties.get( "logLevel" ).asInstanceOf[ String ] ) )

    // -------------------

    val scalatronAkkaServerIp = readRequiredStringProperty( "scalatronAkkaServerIp" )
    val scalatronAkkaServerPort = readRequiredIntProperty( "scalatronAkkaServerPort" )

    val maxConnectingRetriesCount = readRequiredIntProperty( "maxConnectingRetriesCount" )
    val dealyBetweenReconnectionsInMs = readRequiredIntProperty( "dealyBetweenReconnectionsInMs" )

    val stringFromScalatronServerMaxLength = readRequiredIntProperty( "stringFromScalatronServerMaxLength" )
    val stringToScalatronServerMaxLength = readRequiredIntProperty( "stringToScalatronServerMaxLength" )
    val tcpIpReadBufferSize = readRequiredIntProperty( "tcpIpReadBufferSize" )

    // -------------------

    properties = null

    // -------------------

    protected def readRequiredIntProperty( propertyName : String ) : Int = Integer.parseInt( readRequiredStringProperty( propertyName ) )

    protected def readRequiredStringProperty( propertyName : String ) : String =
    {
      properties.getProperty( propertyName ) match
      {
        case propertyValue : String => propertyValue
        case null => throw new RuntimeException( "Could not find '" + propertyName + "' property" )
      }
    }

    protected def loadProperies( systemPropertyNameThatContainsFileName : String , internalJarFileNameIfThereIsNoSystemProperty : String ) : java.util.Properties =
    {
      val properties = new java.util.Properties

      def determinateInputStream : java.io.InputStream =
      {
        System.getProperty( systemPropertyNameThatContainsFileName ) match
        {
          case fileName : String =>
          {
            log.info( "Will try to load external settings file '" + fileName + "' specified by system property" )
            new java.io.FileInputStream( fileName )
          }

          case null =>
          {
            log.info( "Will try to load JAR internal settings file '" + internalJarFileNameIfThereIsNoSystemProperty + "'" )
            val is = getClass.getClassLoader.getResourceAsStream( internalJarFileNameIfThereIsNoSystemProperty )
            if ( is == null ) throw new Exception( "Could not open internal (inside JAR) settings file '" + internalJarFileNameIfThereIsNoSystemProperty + "'" )
            is
          }
        }
      }

      val is = determinateInputStream
      try
      {
        properties.load( is )
      }
      finally
      {
        try{ is.close }catch{ case e : Exception => }
      }

      properties
    }

    protected def configureLogging( logLevel : Level )
    {
      //val rootLogger = LogManager.getLogManager.getLogger( "" )
      val rootLogger = Logger.getLogger( "main.scala" )
      rootLogger.setLevel( logLevel )
      rootLogger.setUseParentHandlers( false )

      val consoleHanlder = new ConsoleHandler
      consoleHanlder.setLevel( logLevel )
      consoleHanlder.setFormatter( new main.scala.org.kermitas.logging.SimpleFormatter )

      rootLogger.addHandler( consoleHanlder )
    }
  }
}