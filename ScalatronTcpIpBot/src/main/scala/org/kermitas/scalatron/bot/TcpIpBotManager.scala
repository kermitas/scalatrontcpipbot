package main.scala.org.kermitas.scalatron.bot
{
  object TcpIpBotManager
  {
    protected var currentControlFunctionFactory : ControlFunctionFactoryTrait = null
    protected val log = java.util.logging.Logger.getLogger( getClass.getName )
    Settings

    def createNewControlFunction : String => String =
    {
      closeCurrentControlFunction

      log.info( "Creating new control function" )
      currentControlFunctionFactory = main.scala.org.kermitas.scalatron.tcpip.TcpIpControlFunction( Settings.scalatronAkkaServerIp , Settings.scalatronAkkaServerPort , Settings.stringFromScalatronServerMaxLength , Settings.stringToScalatronServerMaxLength , Settings.tcpIpReadBufferSize , Settings.maxConnectingRetriesCount , Settings.dealyBetweenReconnectionsInMs )

      currentControlFunctionFactory.create
    }

    def closeCurrentControlFunction
    {
      if( currentControlFunctionFactory != null )
      {
        log.info( "Closing current control function..." )

        try{ currentControlFunctionFactory.close } catch{ case t : Throwable => }

        currentControlFunctionFactory = null
      }
    }
  }
}