package main.scala.org.kermitas.scalatron.bot
{
  trait ControlFunctionFactoryTrait extends AutoCloseable
  {
    def create : String => String

    override def close {}
  }
}