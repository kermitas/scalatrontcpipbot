package main.scala.org.kermitas.scalatron.tcpip
{
  import java.net._
  import java.util.logging._
import main.scala.org.kermitas.scalatron.bot.{TcpIpBotManager, ControlFunctionFactoryTrait}
import main.scala.org.kermitas.scalatron.bot

  object TcpIpControlFunction
  {
    protected val log = Logger.getLogger( getClass.getName )

    def apply( ip : String , port : Int , stringFromScalatronServerMaxLength : Int , stringToScalatronServerMaxLength : Int , internalReadBufferLength : Int , maxConnectingRetriesCount : Int , dealyBetweenReconnectionsInMs : Int ) =
    {
      new TcpIpControlFunction( connectToRemoteHost( ip , port , maxConnectingRetriesCount , dealyBetweenReconnectionsInMs ) , stringFromScalatronServerMaxLength , stringToScalatronServerMaxLength , internalReadBufferLength )
    }

    protected def connectToRemoteHost( ip : String , port : Int , maxConnectingRetriesCount : Int , dealyBetweenReconnectionsInMs : Int ) : Socket =
    {
      for ( i <- scala.collection.immutable.Range.inclusive(1,maxConnectingRetriesCount) )
      {
        try
        {
          log.fine( "Connecting to " + ip + ":" + port + "... " + i + "/" + maxConnectingRetriesCount )
          return new Socket( ip , port )
        }
        catch
        {
          case e : Exception =>
          {
            log.warning( "Could not connect to " + ip + ":" + port + ", exception = " + e )
            Thread.sleep( dealyBetweenReconnectionsInMs )
          }
        }
      }

      throw new Exception( "Could not connect to " + ip + ":" + port + " (during all " + maxConnectingRetriesCount + " attempts)" )
    }
  }

  case class TcpIpControlFunction( socket : Socket , stringFromScalatronServerMaxLength : Int , stringToScalatronServerMaxLength : Int , internalReadBufferLength : Int ) extends ControlFunctionFactoryTrait
  {
    import java.io._

    protected val log = Logger.getLogger( getClass.getName )
    protected val dataOutputStream = new DataOutputStream( socket.getOutputStream )
    protected val dataInputStream = new DataInputStream( socket.getInputStream )
    protected val internalReadBuffer = new Array[ Byte ]( internalReadBufferLength )

    protected var scalatronServerControlFunctionExcecutionCount = 0L
    protected var controlFunctionCreationTime = 0L

    override def create : String => String =
    {
      controlFunctionCreationTime = System.currentTimeMillis

      log.fine( "Returning new control function requested by Scalatron server (control function creation time " + controlFunctionCreationTime + " ms)" )
      controlFunction
    }

    protected def controlFunction( stringFromScalatronServer : String ) : String =
    {
      this.synchronized
      {
        try
        {
          scalatronServerControlFunctionExcecutionCount +=1

          writeStringToRemoteHost( stringFromScalatronServer )

          if ( stringFromScalatronServer.startsWith( "Goodbye" ) )
          {
            log.info( "Close requested by Scalatron server ('Goodbye' message)..." )
            main.scala.org.kermitas.scalatron.bot.TcpIpBotManager.closeCurrentControlFunction
            ""
          }
          else
            readStringFromRemoteHost
        }
        catch
        {
          case e : Exception =>
          {
            log.log( Level.WARNING , "Caught exception '" + e + "', closing current connection" , e )
            bot.TcpIpBotManager.closeCurrentControlFunction
            throw e
          }
        }
      }
    }

    protected def writeStringToRemoteHost( stringFromScalatronServer : String )
    {
      if ( stringFromScalatronServer.length > stringFromScalatronServerMaxLength ) throw new IllegalArgumentException( "String from Scalatron server is too long (current length = " + stringFromScalatronServer.length + ", max length = " + stringFromScalatronServerMaxLength + ")" )

      log.finest( "Sending to remote host: " + stringFromScalatronServer )

      dataOutputStream.writeInt( stringFromScalatronServer.length )
      //dataOutputStream.flush

      dataOutputStream.write( stringFromScalatronServer.getBytes )
      dataOutputStream.flush
    }

    protected def readStringFromRemoteHost : String =
    {
      val stringToScalatronServerLength = dataInputStream.readInt
      if ( stringToScalatronServerLength > stringToScalatronServerMaxLength ) throw new IllegalArgumentException( "String to Scalatron server is too long (current length = " + stringToScalatronServerLength + ", max length = " + stringToScalatronServerMaxLength + ")" )
      readStringFromRemoteHost( stringToScalatronServerLength )
    }

    protected def readStringFromRemoteHost( stringToScalatronServerLength : Int ) : String =
    {
      val byteBuffer = java.nio.ByteBuffer.allocate( stringToScalatronServerLength )

      while( byteBuffer.remaining > 0 )
      {
        val dataChunkLength = socket.getInputStream.read( internalReadBuffer )

        if ( dataChunkLength == -1 )
          throw new IOException( "Could not read from input stream" )
        else
          byteBuffer.put( internalReadBuffer , 0 , dataChunkLength )
      }

      val stringToScalatronServer = new String( byteBuffer.array )

      log.finest( "Sending to Scalatron server: " + stringToScalatronServer )

      stringToScalatronServer
    }

    override def close
    {
      val time = System.currentTimeMillis - controlFunctionCreationTime

      var seconds = time / 1000
      if ( seconds == 0 ) seconds = 1

      val executionsPerSecond = scalatronServerControlFunctionExcecutionCount / seconds

      log.info( "Closing connection " + socket + "; statistics: control function executions count " + scalatronServerControlFunctionExcecutionCount + " in " + time + " ms (" + executionsPerSecond + " executions per second)" )

      try{ socket.close }catch{ case e : Exception => }

      super.close
    }
  }
}