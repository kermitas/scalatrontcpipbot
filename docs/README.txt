
Welcome in Scalatron TCP/IP bot.

// --------------------------------

To use external settings file use "main.scala.org.kermitas.scalatron.bot.tcpip.Settings" system property.

// --------------------------------

Example:

java -Dmain.scala.org.kermitas.scalatron.bot.tcpip.Settings=/some/path/scalatron-1.1.0.2/bots/arthur/settings.conf -jar Scalatron.jar

// --------------------------------

Please see /src/.../resources folder for configuration files.

// --------------------------------
